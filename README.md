# Nameko InfluxDB Metrics

A [nameko](http://nameko.readthedocs.io/) microservice to **write/read** **to/from** influxdb (inspired by Grafana query builder).

## Docs

+ [Setup](docs/setup.md)
+ [Deployment](docs/deployment.md)

## Stack

+ [RabbitMQ](https://www.rabbitmq.com/) -> Message broker for nameko rpc
+ [Logstash](https://www.elastic.co/products/logstash) -> Instances of the nameko rpc are designed to be stateless and ephimeral. Log collection happens through a logstash pipeline
+ [ElasticSearch](https://www.elastic.co/products/elasticsearch) -> Logstash pipeline sends data to elasticsearch for indexing

You can use whatever you want to visualize the logs. To ease development, there are [Kibana](https://www.elastic.co/products/kibana) and [Grafana](http://grafana.com/) services available as part of the `docker-compose` setup. Please take a look at the [Setup Docs](docs/setup.md)


## How to use

#### Write single point

```python
from nameko.standalone.rpc import ClusterRpcProxy

config = {
    'AMQP_URI': "amqp://admin:admin123@0.0.0.0",
}

measurement = 'temp_n_humidity'

fields = {
  'temperature': 20.4,
  'humidity': 30
}

tags = {
  'location': 'living room',
  'sensor': '18b20',
}

with ClusterRpcProxy(config) as cluster_rpc:
    cluster_rpc.timemetrics.write(measurement, fields=fields, tags=tags)
```

### Write multiple points at once

```python
measurement = 'temp_n_humidity'
points = [
  {
    'fields': {
      'temperature': 18.5,
      'humidity': 90,
    },
    'tags': {
      'location': 'bathroom',
      'sensor' 'temp_n_humidity',
    }
  },
  {
    'fields': {
      'temperature': 20.4,
      'humidity': 30,
    },
    'tags': {
      'location': 'living room',
      'sensor': '18b20',
    }
  }
]
with ClusterRpcProxy(config) as cluster_rpc:
    cluster_rpc.timemetrics.write(measurement, points)
```

### Read all points from a measurement

> This is not smart at all if the measurement has a LOT of points.

```python
measurement = 'temp_n_humidity'
with ClusterRpcProxy(config) as cluster_rpc:
    results = cluster_rpc.timemetrics.read(measurement)

```

### Aggregate points

#### Aggregation on single field

```python
measurement = 'temp_n_humidity'
field = 'temperature'
aggregation = 'sum'
with ClusterRpcProxy(config) as cluster_rpc:
    results = cluster_rpc.timemetrics.read(
        measurement,
        select=field,
        aggregation=aggregation
    )
```

#### Aggregation on multiple fields

```python
measurement = 'temp_n_humidity'
fields = ['temperature', 'humidity']
aggregation = ['sum', 'avg']
with ClusterRpcProxy(config) as cluster_rpc:
    results = cluster_rpc.timemetrics.read(
        measurement,
        select=fields,
        aggregation=aggregations
  )
```

This will apply `sum` on `temperature` and `avg` on `humidity`.

### Filter points

#### Filter on time range

If you send a single timestamp then it assumes you want points older than the point.

```python
measurement = 'temp_n_humidity'
field = 'temperature'
time_range=[
    datetime.utcnow().replace(tzinfo=pytz.utc) - timedelta(hours=12)
]
with ClusterRpcProxy(config) as cluster_rpc:
    results = cluster_rpc.timemetrics.read(
        measurement,
        select=field,
        time_range=time_range
    )
```

If you send two timestamps then you'll get all the points contained by the range.

```python
measurement = 'temp_n_humidity'
field = 'temperature'
time_range=[
    datetime.utcnow().replace(tzinfo=pytz.utc) - timedelta(hours=12)
    datetime.utcnow().replace(tzinfo=pytz.utc) - timedelta(hours=4)
]
with ClusterRpcProxy(config) as cluster_rpc:
    results = cluster_rpc.timemetrics.read(
        measurement,
        select=field,
        time_range=time_range
    )
```

Order of the timestamps does not matter, the client sorts out which one is older.

#### Filter on value of a tag

Single tag

```python
measurement = 'temp_n_humidity'
field = 'temperature'

filter_by = {
  'tag': 'sensor',
  'operator': '=',
  'value': '18b20'
}

with ClusterRpcProxy(config) as cluster_rpc:
    results = cluster_rpc.timemetrics.read(
        measurement,
        select=field,
        filter_by=filter_by
    )
```

Multiple tags


```python
measurement = 'temp_n_humidity'
field = 'temperature'

filter_by = [
    {
        'tag': 'sensor',
        'operator': '=',
        'value': '18b20'
    },
    {
        'tag': 'location',
        'operator': '=',
        'value': 'living room',
    }
]

with ClusterRpcProxy(config) as cluster_rpc:
    results = cluster_rpc.timemetrics.read(
        measurement,
        select=field,
        filter_by=filter_by
    )
```

### Group results

#### Group in blocks of time

```python
measurement = 'temp_n_humidity'
field = 'temperature'
aggregation = 'avg'
time_block = '1h'

with ClusterRpcProxy(config) as cluster_rpc:
    results = cluster_rpc.timemetrics.read(
        measurement,
        select=field,
        time_block=time_block
    )
```

#### Group by tag

Single tag

```python
measurement = 'temp_n_humidity'
field = 'temperature'
aggregation = 'avg'
group_by = 'location'

with ClusterRpcProxy(config) as cluster_rpc:
    results = cluster_rpc.timemetrics.read(
        measurement,
        select=field,
        group_by=time_block
    )
```

Multiple tags

```python
measurement = 'temp_n_humidity'
field = 'temperature'
aggregation = 'avg'
group_by = ['location', 'sensor']

with ClusterRpcProxy(config) as cluster_rpc:
    results = cluster_rpc.timemetrics.read(
        measurement,
        select=field,
        group_by=time_block
    )
```

