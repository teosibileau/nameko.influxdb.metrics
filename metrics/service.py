# import json
from nameko.rpc import rpc
from grafane import Grafane
try:
    from utils import LogstashDependency
except ModuleNotFoundError:
    from .utils import LogstashDependency


class InfluxdbMetricsService:
    name = "timemetrics"
    log = LogstashDependency()

    @rpc
    def write(self, measurement, fields, tags, timestamp=False):
        p = {
            'fields': fields,
            'tags': tags
        }
        if timestamp:
            p['time'] = timestamp
        self.write_points(measurement, [p])

    @rpc
    def write_points(self, measurement, points):
        c = Grafane(measurement)
        r = c.report_points(points)
        log_extra = {
            'measurement': measurement,
            'success': r,
        }
        self.log.info('Writing points', extra=log_extra)
        return r

    @rpc
    def read(self, measurement, select='value', **kwargs):
        c = Grafane(measurement)
        if select and 'aggregation' in kwargs:
            c.select(select, kwargs['aggregation'])
        elif select:
            c.select(select)
        if 'time_range' in kwargs:
            c.filter_time_range(kwargs['time_range'])
        if 'filter_by' in kwargs:
            c.filter_by_from_dict(kwargs['filter_by'])
        if 'group_by' in kwargs:
            c.group_by(kwargs['group_by'])
        if 'time_block' in kwargs:
            c.time_block(kwargs['time_block'])
        log_extra = {
            'measurement': measurement,
            'sql': c.sql
        }
        self.log.info('Querying database', extra=log_extra)
        return c.execute_query()

    @rpc
    def raw(self, sql):
        c = Grafane()
        return c.query(sql)

    @rpc
    def drop(self, measurement):
        c = Grafane(measurement)
        r = c.drop_measurement()
        log_extra = {
            'measurement': measurement,
            'response': str(r)
        }
        self.log.info('Droping measurement', extra=log_extra)
        return r
