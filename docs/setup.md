# Setup

+ [Docker](https://docs.docker.com/install/)
+ [Docker Compose](https://docs.docker.com/compose/install/)
+ [ahoy](https://github.com/ahoy-cli/ahoy#installation)


## Development using containerized nameko rpc

+ Build container for the nameko rpc

```
➔ ahoy dev docker build
###############################################################################################
docker-compose -f docker-compose.yml -f docker-compose.dev.yml -p nameko.influxdb.metrics build
###############################################################################################

metrics uses an image, skipping
broker uses an image, skipping
dashboards uses an image, skipping
elasticsearch uses an image, skipping
kibana uses an image, skipping
logstash uses an image, skipping
Building service
Step 1/15 : FROM python:3.6-jessie
 ---> 82bc061397b6
Step 2/15 : MAINTAINER Teo Sibileau
 ---> Using cache
 ---> feb0718e9b36
Step 3/15 : RUN apt-get update     && apt-get -y install git                           python3-dev                           python3-pip                           libfontconfig                           libssl-dev                           build-essential                           libffi-dev                           software-properties-common
 ---> Using cache
 ---> 440d2e3d0eaa
Step 4/15 : RUN pip3 install --upgrade pip
 ---> Using cache
 ---> b04735f4fca0
Step 5/15 : ARG ENV=PROD
 ---> Using cache
 ---> 966cf933e832
Step 6/15 : RUN echo "Building image for $ENV environment"
 ---> Using cache
 ---> b1309a6cad5f
Step 7/15 : WORKDIR /code
 ---> Using cache
 ---> 2366d04f1593
Step 8/15 : ADD requirements.txt /code
 ---> 6e67f90d9cde
Step 9/15 : ADD requirements.dev.txt /code
 ---> 6dd38ad985b2
Step 10/15 : ADD .docker/install_dev_requirements.sh /code
 ---> 2cfccb49ba42
Step 11/15 : RUN pip3 install -r requirements.txt
 ---> Running in 8a0d5fcfaf29
```

Start containers

```
➔ ahoy dev docker up
################################################################################################################
docker-compose -f docker-compose.yml -f docker-compose.dev.yml -p nameko.influxdb.metrics up --remove-orphans -d
################################################################################################################

Starting namekoinfluxdbmetrics_elasticsearch_1 ... done
Starting namekoinfluxdbmetrics_dashboards_1    ... done
Starting namekoinfluxdbmetrics_broker_1        ... done
Starting namekoinfluxdbmetrics_metrics_1       ... done
Starting namekoinfluxdbmetrics_kibana_1        ... done
Starting namekoinfluxdbmetrics_logstash_1      ... done
Starting namekoinfluxdbmetrics_service_1       ... done
```

## Development using host's python

+ Install [pipenv](https://pipenv.readthedocs.io/en/latest/) (seriously)
+ Create environment

```
➔ pipenv install
Installing dependencies from Pipfile.lock (feaf67)...
  🐍   ▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉ 26/26 — 00:00:09
To activate this project's virtualenv, run pipenv shell.
Alternatively, run a command inside the virtualenv with pipenv run.
```

+ Start all containers but nameko rpc:

```
ahoy dev docker up broker metrics logstash elasticsearch
```

+ Run service

```
➔ cd metrics
➔ pipenv run nameko run --config ./service.yml service

starting <QueueConsumer at 0x10f96e550>
waiting for consumer ready <QueueConsumer at 0x10f96e550>
Connected to amqp://admin:**@0.0.0.0:5672//
setting up consumers <QueueConsumer at 0x10f96e550>
consumer started <QueueConsumer at 0x10f96e550>
started <QueueConsumer at 0x10f96e550>
```
