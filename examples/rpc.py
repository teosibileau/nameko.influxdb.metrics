from random import randint
from nameko.standalone.rpc import ClusterRpcProxy

config = {
    'AMQP_URI': "amqp://admin:admin123@0.0.0.0",
}

with ClusterRpcProxy(config) as cluster_rpc:
    cluster_rpc.timemetrics.write('test', fields={'value': randint(0, 10)},
                                  tags={'t': 'v-%s' % randint(0, 2)})
    results = cluster_rpc.timemetrics.read('test')

print(results)
