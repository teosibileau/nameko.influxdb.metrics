-i https://pypi.org/simple
amqp==2.4.1; python_version != '3.2.*'
certifi==2018.11.29
chardet==3.0.4
dnspython==1.16.0; python_version != '3.2.*'
enum-compat==0.0.2
eventlet==0.20.1
grafane==0.5
greenlet==0.4.15
idna==2.8; python_version != '3.0.*'
importlib-metadata==0.8; python_version >= '2.7'
influxdb==5.2.1
kombu==4.3.0; python_version != '3.2.*'
limits==1.3; python_version != '3.0.*'
mock==2.0.0; python_version != '3.1.*'
monotonic==1.5
nameko==2.11.0
path.py==11.5.0; python_version >= '2.7'
pbr==5.1.2
pylogbeat==1.0.2; python_version != '3.0.*'
python-dateutil==2.8.0
python-logstash-async==1.5.0
pytz==2018.9
pyyaml==3.13
requests==2.21.0
six==1.12.0; python_version != '3.1.*'
urllib3==1.24.1; python_version != '3.0.*'
vine==1.2.0; python_version != '3.2.*'
werkzeug==0.14.1
wrapt==1.11.1
zipp==0.3.3; python_version >= '2.7'
